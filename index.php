<?php
$access_token = "EAAWiY7QrE5gBAAoHc3Y4S83efKQ5pzZAmbI2sZAKDkekit9okWEDZB7GWVGsxHbZCBEhgIG5q5pZAnAr2ebjWjtCpNGHJhVe3QFQNXCATz5MlnuOUK11ajC1JJz93OW8xUuUaEXOtIPXASIn0yX6cQKOC86UjpxmubZBtKe5cbbAZDZD";
$verify_token = "fb_time_bot";
$hub_verify_token = null;
if(isset($_REQUEST['hub_challenge'])) {
    $challenge = $_REQUEST['hub_challenge'];
    $hub_verify_token = $_REQUEST['hub_verify_token'];
}
if ($hub_verify_token === $verify_token) {
    echo $challenge;
}
$input = json_decode(file_get_contents('php://input'), true);
$sender = $input['entry'][0]['messaging'][0]['sender']['id'];
$message = $input['entry'][0]['messaging'][0]['message']['text'];
$message_to_reply = '';

if (!empty($input['entry'][0]['messaging'][0]['postback']['payload'])) {
    $message = $input['entry'][0]['messaging'][0]['postback']['payload'];
}

/* DB Connectivity */
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "biba";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    $message_to_reply = 'Shit! something went wrong.';
}
/* DB Connectivity */

/* Variables */
$image = false;
$imageDetail = false;
$menuList = false;
$styleTips = [
    'Stay away from lycra leggings and befriend classy and comfortable linen or cotton cigarette pants.',
    'Go for kurtas that have A-line silhouettes, and skip the body-hugging ones as they tend to highlight your chubby areas.',
    'You could wear an orange and white or classic black and white striped kurta over cream cotton churidars. In order to achieve a balanced look, make sure to never sport vertical stripes in more than three colours.',
    'Opt for A-line kurtis with embroidery on the empire waist or simply go for sleek metallic belts over asymmetrical flowing kurtas.'
];

/* Some Basic rules to validate incoming messages */
if(preg_match('[time|current time|now]', strtolower($message))) {
    // Make request to Time API
    ini_set('user_agent','Mozilla/4.0 (compatible; MSIE 6.0)');
    $result = file_get_contents("http://www.timeapi.org/utc/now?format=%25a%20%25b%20%25d%20%25I:%25M:%25S%20%25Y");
    if($result != '') {
        $message_to_reply = $result;
    }
} else if(preg_match('[\bhi\b|\bhello\b|\bhey\b|\byo\b]', strtolower($message))) {
    $userInfoURL = 'https://graph.facebook.com/v2.6/' . $sender . '?fields=first_name,last_name,gender&access_token=' . $access_token;
    $curlUser = curl_init($userInfoURL);
    curl_setopt($curlUser, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($curlUser, CURLOPT_RETURNTRANSFER, 1);
    $userResponse = curl_exec($curlUser);
    $userInfo = json_decode($userResponse, true);
    curl_close($curlUser);
    
    $message_to_reply = 'Yo ' . $userInfo['first_name'] . ' ' . $userInfo['last_name'] . '! What Up Man';
} else if(preg_match('[white|white dress]', strtolower($message))) {
    if(preg_match('[detail|info|information]', strtolower($message))){
        $imageDetail = true;
    }
    
    $image = true;
    $color = 'white';
} else if(preg_match('[red|red dress]', strtolower($message))) {
    if(preg_match('[detail|info|information]', strtolower($message))){
        $imageDetail = true;
    }
    
    $image = true;
    $color = 'red';
} else if(preg_match('[catalogue|category|categories]', strtolower($message))) {
    $sql = "SELECT id, name FROM categories";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        // output data of each row
        $message_to_reply = 'Biba Categories:\n';
        while($row = $result->fetch_assoc()) {
            $message_to_reply .= "#" . $row["id"] . ": " . $row["name"] . '\n';
        }
    } else {
        $message_to_reply = "0 results";
    }
} else if(preg_match('[style tips|tips|tricks]', strtolower($message))) {
    $randomTips = array_rand($styleTips, 1);
    $message_to_reply = $styleTips[$randomTips];
} else if(preg_match('[\bhelp\b|\bmenu\b]', strtolower($message))) {
    $menuList = true;
}else {
    $message_to_reply = 'Huh! what do you mean?';
}

//API Url
$url = 'https://graph.facebook.com/v2.6/me/messages?access_token='.$access_token;
//Initiate cURL.
$ch = curl_init($url);
//The JSON data.
if (!$image){
    if($menuList){
        $jsonData = '{
            "recipient":{
                "id":"'.$sender.'"
            },
            "message":{
                "attachment":{
                    "type":"template",
                    "payload":{
                      "template_type":"button",
                      "text":"We have these option(s):",
                      "buttons":[{
                          "type":"postback",
                          "title":"Style Tips",
                          "payload":"style tips"
                        },{
                          "type":"postback",
                          "title":"Info: White Dress",
                          "payload":"white dress info"
                        },{
                          "type":"postback",
                          "title":"Info: Red Dress",
                          "payload":"red dress info"
                        }
                      ]
                    }
                  }
                }
            }';
    } else {
        $jsonData = '{
            "recipient":{
                "id":"'.$sender.'"
            },
            "message":{
                "text":"'.$message_to_reply.'"
            }
        }';
    }
} else {
    if ($imageDetail){
        $dressURL = '';
        if($color == 'red'){
            $dressURL = 'http://www.biba.in/item/red-flared-cotton-dress-tanabana11432red.html';
        } else {
            $dressURL = 'http://www.biba.in/item/white-kalidar-cotton-dress-indigof11504wht.html';
        }
        
        $jsonData = '{
            "recipient":{
                "id":"'.$sender.'"
            },
            "message":{
                "attachment":{
                    "type":"template",
                    "payload":{
                        "template_type":"generic",
                        "elements":[
                            {
                                "title":"Classic '. ucfirst($color) .' Dress",
                                "image_url":"https://5bdc20ce.ngrok.io/' . $color . '.jpg",
                                "subtitle":"Soft ' . $color . ' dress is back in style",
                                "buttons":[{
                                  "type":"web_url",
                                  "url": "' . $dressURL . '",
                                  "title":"View Item"
                                }]
                            }
                        ]
                    }
                }
            }
        }';
    } else {
        $jsonData = '{
        "recipient":{
            "id":"'.$sender.'"
        },
        "message":{
            "attachment":{
                "type":"image",
                "payload":{
                  "url":"https://5bdc20ce.ngrok.io/' . $color . '.jpg"
                }
            }
        }
    }';
    }
}
//Encode the array into JSON.
$jsonDataEncoded = $jsonData;
//Tell cURL that we want to send a POST request.
curl_setopt($ch, CURLOPT_POST, 1);
//Attach our encoded JSON string to the POST fields.
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
//Execute the request
if(!empty($input['entry'][0]['messaging'][0]['message']) || !empty($input['entry'][0]['messaging'][0]['postback']['payload'])){
    $result = curl_exec($ch);
}

$conn->close();